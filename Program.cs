﻿using System;
using Visitor.Entities;
using Visitor.Interfaces;
using Visitor.Serializers;

namespace Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            IVisitor visitor = new VisitorXML();

            Shape shape = new Shape(10, 10);
            Shape circle = new Circle(10, 11, 12);
            Shape rectangle = new Rectangle(34, 12, 10, 10);

            Console.WriteLine(shape.Export(visitor));
            Console.WriteLine(circle.Export(visitor));
            Console.WriteLine(rectangle.Export(visitor));

            visitor = new VisitorJSON();

            Console.WriteLine(shape.Export(visitor));
            Console.WriteLine(circle.Export(visitor));
            Console.WriteLine(rectangle.Export(visitor));

            Console.ReadLine();
        }
    }
}
