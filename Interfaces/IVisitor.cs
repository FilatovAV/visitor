﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Visitor.Interfaces
{
    public interface IVisitor
    {
        string Export(Entities.Shape shape);
        string Export(Entities.Rectangle shape);
        string Export(Entities.Circle shape);
    }
}
