﻿using System;
using System.Collections.Generic;
using System.Text;
using Visitor.Interfaces;

namespace Visitor.Entities
{
    public class Circle: Shape
    {
        public double Radius { get; set; }

        public Circle(double x, double y, double radius): base(x, y)
        {
            Radius = radius;
        }
        public override string Export(IVisitor visitor) => visitor.Export(this);
    }
}
