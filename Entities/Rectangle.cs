﻿using System;
using System.Collections.Generic;
using System.Text;
using Visitor.Interfaces;

namespace Visitor.Entities
{
    public class Rectangle: Shape
    {
        public double With { get; set; }
        public double Height { get; set; }
        public Rectangle(double x, double y, double width, double height) : base(x, y)
        {
            With = width;
            Height = height;
        }
        public override string Export(IVisitor visitor) => visitor.Export(this);
    }
}
