﻿using System;
using System.Collections.Generic;
using System.Text;
using Visitor.Interfaces;

namespace Visitor.Entities
{
    public class Shape
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Shape(double x, double y)
        {
            X = x;
            Y = y;
        }

        public virtual string Export(IVisitor visitor) => visitor.Export(this);
    }
}
