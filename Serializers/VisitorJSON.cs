﻿using System;
using System.Collections.Generic;
using System.Text;
using Visitor.Entities;
using Visitor.Interfaces;

namespace Visitor.Serializers
{
    public class VisitorJSON: IVisitor
    {
        public string Export(Shape shape)
        {
            return $@"{{""X"": {shape.X:F1}, ""Y"": {shape.Y:F1}}}";
        }

        public string Export(Rectangle shape)
        {
            return $@"{{""X"": {shape.X:F1}, ""Y"": {shape.Y:F1}, ""Height"": {shape.Height:F1}, ""With"": {shape.With:F1}}}";
        }

        public string Export(Circle shape)
        {
            return $@"{{""X"": {shape.X:F1}, ""Y"": {shape.Y:F1}, ""Radius"": {shape.Radius:F1}}}";
        }
    }
}
