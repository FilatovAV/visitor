﻿using System;
using System.Collections.Generic;
using System.Text;
using Visitor.Entities;
using Visitor.Interfaces;

namespace Visitor.Serializers
{
    public class VisitorXML : IVisitor
    {
        public string Export(Shape shape)
        {
            return $@"<SHAPE>
                        <X>{shape.X}</X>
                        <Y>{shape.Y}</Y>
                    </SHAPE>";
        }

        public string Export(Rectangle shape)
        {
            return $@"<RECTANGLE>
                        <X>{shape.X}</X>
                        <Y>{shape.Y}</Y>
                        <H>{shape.Height}>/H>
                        <W>{shape.With}</W>
                    </RECTANGLE>";
        }

        public string Export(Circle shape)
        {
            return $@"<CIRCLE>
                        <X>{shape.X}</X>
                        <Y>{shape.Y}</Y>
                        <R>{shape.Radius}>/R>
                    </CIRCLE>";
        }
    }
}
